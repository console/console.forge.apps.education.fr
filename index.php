<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CONSOLE</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  </head>

<body>

<?php
// Désactiver les rapports d'erreurs
error_reporting(0);

// Remplacez ces valeurs par votre propre URL GitLab et votre jeton d'accès
define('GITLAB_URL', 'https://forge.apps.education.fr');
define('ACCESS_TOKEN', getenv('SECRET_VARIABLE'));
define('MIN_COMMITS', 20); // Nombre minimum de commits
define('DAYS_AGO', 30); // Nombre de jours



function get_active_repositories() {
    
    $active_directories = [];

    // Date il y a 30 jours
    $start_date = date('Y-m-d\TH:i:s.v\Z', strtotime('-' . DAYS_AGO . ' days'));

    // Endpoint pour récupérer tous les projets de GitLab
    $projects_endpoint = GITLAB_URL . '/api/v4/projects';

    // Paramètres de la requête pour trier les projets par activité
    $params = [
        'order_by' => 'last_activity_at',
        'sort' => 'desc'
    ];

    // Entête d'authentification avec le jeton d'accès
    $headers = [
        'Private-Token' => ACCESS_TOKEN
    ];

    // Envoyer la requête GET pour récupérer les projets
    $response = file_get_contents($projects_endpoint . '?' . http_build_query($params), false, stream_context_create([
        'http' => [
            'header' => 'Private-Token: ' . ACCESS_TOKEN
        ]
    ]));

    // Vérifier si la requête a réussi (code 200)
    if ($response !== false) {
        // Récupérer la liste des projets depuis la réponse JSON
        $projects = json_decode($response, true);

        // Filtrer les projets en fonction du nombre minimum de commits au cours des 30 derniers jours
        foreach ($projects as $project) {
            $commits = [];
            $page = 1;
            while (true) {
                // Endpoint pour récupérer les commits du projet
                $commits_endpoint = GITLAB_URL . '/api/v4/projects/' . $project['id'] . '/repository/commits';

                $params = [
                    'since' => $start_date,
                    'per_page' => 100,
                    'page' => $page
                ];
                $commits_response = file_get_contents($commits_endpoint . '?' . http_build_query($params), false, stream_context_create([
                    'http' => [
                        'header' => 'Private-Token: ' . ACCESS_TOKEN
                    ]
                ]));
                if ($commits_response !== false) {
                    $commits_page = json_decode($commits_response, true);
                    if (empty($commits_page)) {
                        break;
                    }
                    $commits = array_merge($commits, $commits_page);
                    $page++;
                } else {
                    //echo "Erreur lors de la récupération des utilisateurs.";
                    break;
                }
            }

            // Vérifier si le nombre de commits est supérieur ou égal au minimum requis
            if (count($commits) >= MIN_COMMITS) {
                $active_directories[] = [$project, count($commits)]; 
                //echo $project['name_with_namespace'], ' - Nb de commits: ', count($commits), PHP_EOL;
                //echo $project['name_with_namespace'], ' - Nb de commits: ', count($commits), PHP_EOL;
            }
        }
    } else {
        //echo 'Erreur: Impossible de récupérer les projets', PHP_EOL;
    }

    return $active_directories;
}
?>

<div class="container mt-5">
    <div class="row">
        <div class="col">
            <?php
            $active_directories = get_active_repositories();
            $active_directories = array_slice($active_directories, 0, 12);

            
            echo "<h2>Les 12 projets les plus actifs</h2>";
            echo "<table class='table table-borderless table-hover table-sm font-monospace small'>";
            foreach ($active_directories as $active_directory) {
                echo "<tr>";
                echo "<td>";
                if ($active_directory[0]['avatar_url']) {
                    echo "<img class='m-1' style='border-radius: 50%;' src='" . $active_directory[0]['avatar_url'] . "' width='24' />";
                } else {
                    echo "<img class='m-1' style='border-radius: 50%;' src='https://forge.apps.education.fr/docs/docs.forge.apps.education.fr/-/raw/main/docs/assets/images/favicon_merge.png' width='24' />";
                } 
                    echo "<a href='" . $active_directory[0]['web_url'] . "' target='_blank'>" . $active_directory[0]['name_with_namespace'] . "</a>";
                if ($active_directory[0]['description']) echo "<div class='text-muted' style='padding-left:32px'>" . $active_directory[0]['description'] . "</div>"; 
                echo "</td>";
                echo "<td>" . $active_directory[1] . "</td>";
                echo "</tr>";

            }
            echo "</table>";

            echo "<pre>";
            //print_r($active_directories);
            echo "</pre>";
            ?>
        </div>
    </div>
</div>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-YvpcrYf0tY3lHB60NNkmXc5s9fDVZLESaAA55NDzOxhy9GkcIdslK1eN7N6jIeHz" crossorigin="anonymous"></script>
  </body>
</html>